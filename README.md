Projet Simple Bio
===

##### Réalisé dans le cadre du module 'Programmation Orientée Objet'
##### Professeurs : OUSSALAH Chabane, QUEUDET Audrey et TONNEAU Quentin 
##### Auteur : _CHETTA Aline_ et _NAULLET Guillaume_

## But :

- Appréhender la programmation orientée objet
- Réprésentation de façon simplifiée des éléments de la biologie et leurs intéractions
- Créer une pseudo Base de données

## Pré-requis :
- Environnement
    - Testé sous Ubuntu 17.10
    - Compilateur C++11


## Éxécuter l'application :

- ` cd cheminDuProjet `
- ` cmake . `
- ` make `
- ` ./bddCell `

## Source

Thanks to **_nlohmann_** for is JSON Library : (https://github.com/nlohmann/json)
