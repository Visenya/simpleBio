//
// Created by Aline et Guillaume on 26/11/17.
//

#ifndef BDDCELL_DATABASE_H
#define BDDCELL_DATABASE_H

#include <string>
#include <iostream>
#include <list>
#include <algorithm>

#include "Eukaryote.h"
#include "Prokaryote.h"
#include "Cell.h"
#include "../lib/json.hpp"


using namespace nlohmann;
using namespace std;

class Database {

private:
    list<Eukaryote> arrayEukaryote;
    list<Prokaryote> arrayProkaryote;

public:

    /**
     * Get array Eukaryote
     * @return list<Eukaryote>
     */
    list<Eukaryote> getArrayEukaryote() const ;


    /**
     * Set array Eukaryote
     * @param arrayEukaryote
     */
    void setArrayEukaryote(list<Eukaryote> arrayEukaryote);


    /**
     * Get array Prokaryote
     * @return list<Prokaryote>
     */
    list<Prokaryote> getArrayProkaryote() const ;


    /**
     * Set array Prokaryote
     * @param arrayProkaryote
     */
    void setArrayProkaryote(list<Prokaryote> arrayProkaryote);


    /**
     * Add Eukaryote
     * @param euk
     */
    void addEukaryote(Eukaryote euk);


    /**
     * Add Prokaryote by id
     * @param pro
     */
    void addProkaryote(Prokaryote pro);


    /**
     * Delete Eukaryote by id
     * @param id
     */
    void delEukaryoteById(string id);


    /**
     * Delete Prokaryote
     * @param id
     */
    void delProkaryoteById(string id);


    /**
     * Delete a cell by id
     * @param id
     */
    void delCellById(string id);


    /**
    * Display
    * @param out
    */
    virtual void affiche(ostream & out) const;

    // Fct with filter


    /**
     * Display Cell by Id
     * @param id
     */
    void displayCellById(string id);

    /**
     * Display Prokaryote by Id
     * @param id
     */
    void displayProkaryoteById(string id);

    /**
     * Display Eukaryote by Id
     * @param id
     */
    void displayEukaryoteById(string id);


    /**
     * Display all Prokaryote
     */
    void displayOnlyProkaryote();


    /**
     * Display all Eukaryote
     */
    void displayOnlyEukaryote();


    /**
     * Display all Cell between a defined size
     * @param min
     * @param max
     */
    void displayCellSizeBetween(double min, double max);

    /**
     * Display Eukaryote between a defined size
     * @param min
     * @param max
     */
    void displayEukaryoteSizeBetween(double min, double max);

    /**
     * Display Prokaryote between a defined size
     * @param min
     * @param max
     */
    void displayProkaryoteSizeBetween(double min, double max);


    /**
     * Display all Cell having a chromosome with a defined gene
     * @param idGene
     */
    void displayCellHavingChromosomeExprimedGene(string idGene);

    /**
     * Display all Prokaryote having a chromosome with a defined gene
     * @param idGene
     */
    void displayProkaryoteHavingChromosomeExprimedGene(string idGene);

    /**
     * Display all Eukaryote having a chromosome with a defined gene
     * @param idGene
     */
    void displayEukaryoteHavingChromosomeExprimedGene(string idGene);



    /**
     * Display all Cell sort by size
     */
    void displayCellBySize();


};


/**
 * Override operator <<
 * @param out
 * @param d
 * @return
 */
ostream & operator<<(ostream &out, const Database &d);

/**
 * From Chromosome To json
 * @param j
 * @param d
 */
void to_json(json& j, const Database& d);


/**
 * From json to Chromosome
 * @param j
 * @param d
 */
void from_json(const json& j, Database& d);


#endif //BDDCELL_DATABASE_H
