//
// Created by visenya on 26/11/17.
//

#include "Database.h"


/* Getter and Setter */

list<Eukaryote> Database::getArrayEukaryote() const {
    return this->arrayEukaryote;
}

void Database::setArrayEukaryote(list<Eukaryote> arrayEukaryote) {
    this->arrayEukaryote = arrayEukaryote;
}

list<Prokaryote> Database::getArrayProkaryote() const {
    return this->arrayProkaryote;
}

void Database::setArrayProkaryote(list<Prokaryote> arrayProkaryote) {
    this->arrayProkaryote = arrayProkaryote;
}


/* Fonction Add and Del */

void Database::addEukaryote(Eukaryote e) {
    this->arrayEukaryote.push_back(e);
}

void Database::addProkaryote(Prokaryote p) {
    this->arrayProkaryote.push_back(p);
}

void Database::delEukaryoteById(string id) {
    if ( !this->arrayEukaryote.empty() /*&& existCell*/) {
        list<Eukaryote>::iterator it = arrayEukaryote.begin();
        while (it != this->arrayEukaryote.end()){
            if (it->getIdName() == id){
                this->arrayEukaryote.erase(it);
                break;
            }
            it++;
        }
    }
}

void Database::delProkaryoteById(string id) {
    if ( !this->arrayProkaryote.empty() /*&& existCell*/) {
        list<Prokaryote>::iterator it = arrayProkaryote.begin();
        while (it != this->arrayProkaryote.end()){
            if (it->getIdName() == id){
                this->arrayProkaryote.erase(it);
                break;
            }
            it++;
        }
    }
}

void Database::delCellById(string id) {
    delProkaryoteById(id);
    delEukaryoteById(id);
}



/* Fonction with filter */

void Database::displayCellById(string id){
    displayProkaryoteById(id);
    displayEukaryoteById(id);
}

void Database::displayEukaryoteById(string id){
    if ( !this->arrayEukaryote.empty()) {
        list<Eukaryote>::iterator itEukaryote = arrayEukaryote.begin();
        while (itEukaryote != this->arrayEukaryote.end()){
            if (itEukaryote->getIdName() == id) {
                cout << *itEukaryote << endl;
                break;
            }
            itEukaryote++;
        }
    }
}

void Database::displayProkaryoteById(string id){
    if ( !this->arrayProkaryote.empty()) {
        list<Prokaryote>::iterator itProkaryote = arrayProkaryote.begin();
        while (itProkaryote != this->arrayProkaryote.end()){
            if (itProkaryote->getIdName() == id){
                cout << *itProkaryote << endl;
                break;
            }
            itProkaryote++;
        }
    }
}


void Database::displayOnlyEukaryote() {
    if ( !this->arrayEukaryote.empty() ) {
        list<Eukaryote>::iterator it = arrayEukaryote.begin();
        while (it != this->arrayEukaryote.end()){
            cout << *it << endl;
            it++;
        }
    }
}

void Database::displayOnlyProkaryote() {
    if ( !this->arrayProkaryote.empty() /*&& existCell*/) {
        list<Prokaryote>::iterator it = arrayProkaryote.begin();
        while (it != this->arrayProkaryote.end()){
            cout << *it << endl;
            it++;
        }
    }
}

void Database::displayCellSizeBetween(double min, double max) {
    displayProkaryoteSizeBetween(min, max);
    displayEukaryoteSizeBetween(min, max);
}

void Database::displayProkaryoteSizeBetween(double min, double max) {
    if ( !this->arrayProkaryote.empty()) {
        list<Prokaryote>::iterator itProkaryote = arrayProkaryote.begin();

        while (itProkaryote != this->arrayProkaryote.end()){
            if (itProkaryote->getSize() >= min && itProkaryote->getSize() <= max){
                cout << *itProkaryote << endl;
            }
            itProkaryote++;
        }
    }
}

void Database::displayEukaryoteSizeBetween(double min, double max) {
    if (!this->arrayEukaryote.empty()) {
        list<Eukaryote>::iterator itEukaryote = arrayEukaryote.begin();

        while (itEukaryote != this->arrayEukaryote.end()){
            if (itEukaryote->getSize() >= min && itEukaryote->getSize() <= max) {
                cout << *itEukaryote << endl;
            }
            itEukaryote++;
        }
    }
}


void Database::displayProkaryoteHavingChromosomeExprimedGene(string idGene) {
    if ( !this->arrayProkaryote.empty()) {
        for ( auto const& pro : arrayProkaryote){
            if (pro.getChromosome().getNameChromosome() == "") {
            } else{
                for ( auto const& gene : pro.getChromosome().getArrayGene()){
                    if (gene.getNameGene() == idGene){
                        cout << pro << endl;
                    }
                }
            }
        }
    }
}


void Database::displayEukaryoteHavingChromosomeExprimedGene(string idGene) {
    if ( !this->arrayEukaryote.empty()) {
        for ( auto const& euk : arrayEukaryote){
            if (!euk.getArrayChromosome().empty()) {
                for ( auto const& kro : euk.getArrayChromosome()){
                    if (!kro.getArrayGene().empty() ){
                        for ( auto const& gene : kro.getArrayGene()){
                            if (gene.getNameGene() == idGene){
                                cout << euk << endl;
                            }
                        }
                    }
                }
            }
        }
    }
}



void Database::displayCellHavingChromosomeExprimedGene(string idGene) {
    displayProkaryoteHavingChromosomeExprimedGene(idGene);
    displayEukaryoteHavingChromosomeExprimedGene(idGene);
}

bool compCells(Cell* cell, Cell* cellBis) {
    return cell->getSize() < cellBis->getSize();
}


void Database::displayCellBySize() {
    list<Cell*> arrayCell;

    if ( !this->arrayProkaryote.empty()) {
        for (auto &pro : arrayProkaryote) {
            arrayCell.push_back(&pro);
        }
    }

    if ( !this->arrayEukaryote.empty()) {
        for (auto &euk : arrayEukaryote) {
            arrayCell.push_back(&euk);
        }
    }

    arrayCell.sort(compCells);
    if ( !arrayCell.empty()) {
        for (auto const &cell : arrayCell) {
            cout << *cell << endl;
        }
    }

    arrayCell.clear();
}

void Database::affiche(ostream & out) const {
    out << " ---- Database ----- " << endl;
    if (!this->arrayEukaryote.empty()){
        for ( auto const& a : arrayEukaryote)
            out << a;
    }
}


// Override

ostream & operator<<(ostream &out, const Database &c){
    c.affiche(out);
    return out;
}


// Exportation & Importation

void to_json(json& j, const Database& d) {
    j = json{{"arrayEukaryote", d.getArrayEukaryote()}, {"arrayProkaryote", d.getArrayProkaryote()}};
}

void from_json(const json& j, Database& data) {
    data.setArrayEukaryote(j.at("arrayEukaryote").get<std::list<Eukaryote>>());
    data.setArrayProkaryote(j.at("arrayProkaryote").get<std::list<Prokaryote>>());
}

//ranger fichier src/ et co

// Si deux fois Cell présente