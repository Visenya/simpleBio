//
// Created by Aline et Guillaume on 09/11/17.
//

#ifndef BDDCELL_PROTEIN_H
#define BDDCELL_PROTEIN_H

#include <string>
#include <iostream>
#include "../lib/json.hpp"

using namespace std;
using namespace nlohmann;

class Protein {

private :

    string id;
    string name;

public:

    /**
     * Constructor
     * @param id
     * @param name
     */
    Protein(string id = "", string name = "");


    /**
     * Get id
     * @return string
     */
    string getIdProtein() const;


    /**
     * Set id
     * @param id
     */
    void setIdProtein(string id);


    /**
     * Get Name
     * @return string
     */
    string getNameProtein() const;


    /**
     * Set name
     * @param name
     */
    void setNameProtein(string name);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream &out) const;

    /**
     * Override operator ==
     * @param prot
     * @return bool
     */
    bool operator==(Protein prot) const;


    /**
     * Copy Constructor
     * @param prot
     */
    Protein(const Protein& prot) : id(prot.id), name(prot.name){};

};

/**
 * Override operator <<
 * @param out
 * @param p
 * @return
 */
ostream & operator<<(ostream &out, const Protein &p);


/**
 * From Protein To json
 * @param j
 * @param k
 */
void to_json(json& j, const Protein& p);

/**
 * From json to Protein
 * @param j
 * @param k
 */
void from_json(const json& j, Protein& p);

#endif //BDDCELL_PROTEIN_H
