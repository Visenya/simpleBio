//
// Created by Aline et Guillaume on 16/11/17.
//

#ifndef BDDCELL_EUKARYOTE_H
#define BDDCELL_EUKARYOTE_H

#include <iostream>
#include <list>
#include "../lib/json.hpp"
using namespace std;
using namespace nlohmann;
#include "Cell.h"
#include "Chromosome.h"


class Eukaryote : public Cell {

private:
    string nameOrgane;
    list<Chromosome> arrayKro;

public:

    /**
     * Constructor
     * @param id
     * @param size
     * @param nameOrgane
     */
    Eukaryote(string id = "", double size = 0, string nameOrgane = "");


    /**
     * Get name
     * @return string
     */
    string getNameOrgane() const;


    /**
     * Set name
     * @param name
     */
    void setNameOrgane(string name);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream & out) const;


    /**
     * Get array chromosome
     * @return list<Chromosome>
     */
    list<Chromosome> getArrayChromosome() const;


    /**
     * Set arrayKro
     * @param arrayKro
     */
    void setArrayChromosome(list<Chromosome> arrayKro);


    /**
     * Add Chromosome
     * @param kro
     */
    void addChromosome(Chromosome kro);


    /**
     * Override operator ==
     * @param euk
     * @return bool
     */
    bool operator==(Eukaryote euk) const;


    /**
     * Copy Constructor
     * @param euk
     */
    Eukaryote(const Eukaryote& euk);

};


/**
 * Override operator <<
 * @param out
 * @param c
 * @return
 */
ostream & operator<<(ostream &out, const Eukaryote &e);


/**
 * From Eukaryote To json
 * @param j
 * @param g
 */
void to_json(json& j, const Eukaryote& e);


/**
 * From json to Eukaryote
 * @param j
 * @param e
 */
void from_json(const json& j, Eukaryote& e);

#endif //BDDCELL_EUKARYOTE_H
