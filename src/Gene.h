//
// Created by Aline et Guillaume on 20/11/17.
//

#ifndef BDDCELL_GENE_H
#define BDDCELL_GENE_H

#include <string>
#include <iostream>
#include "../lib/json.hpp"

using namespace std;
using namespace nlohmann;

class Gene {

private:
    string nameGene;
    string locus;

public:

    /**
     * Constructor
     * @param nameGene
     * @param locus
     */
    Gene(string nameGene = "", string locus = "");


    /**
     * Get name
     * @return string
     */
    string getNameGene() const;

    /**
     * Get locus
     * @return string
     */
    string getLocus() const;


    /**
     * Set name
     * @param name
     */
    void setNameGene(string name);


    /**
     * Set locus
     * @param locus
     */
    void setLocus(string locus);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream &out) const;


    /**
     * Override operator ==
     * @param gene
     * @return
     */
    bool operator==(Gene gene) const;


    /**
     * Copy Constructor
     * @param gene
     */
    Gene(const Gene& gene) : nameGene(gene.nameGene), locus(gene.locus){};

};


/**
 * Override operator <<
 * @param out
 * @param g
 * @return
 */
ostream & operator<<(ostream &out, const Gene &g);


/**
 * From Gene To json
 * @param j
 * @param g
 */
void to_json(json& j, const Gene& g);

/**
 * From json to Gene
 * @param j
 * @param g
 */
void from_json(const json& j, Gene& g);

#endif //BDDCELL_GENE_H
