//
// Created by Aline et Guillaume on 16/11/17.
//

#ifndef BDDCELL_PROKARYOTE_H
#define BDDCELL_PROKARYOTE_H

#include <iostream>
#include "../lib/json.hpp"
using namespace std;
using namespace nlohmann;


#include "Cell.h"
#include "Chromosome.h"

class Prokaryote : public Cell {

private:
    Chromosome chro;

public:

    /**
     * Constructor
     * @param id
     * @param size
     * @param chromosome
     */
    Prokaryote(string id = "", double size = 0, Chromosome chromosome = Chromosome());


    /**
     * Get Chromosome
     * @return
     */
    Chromosome getChromosome() const;


    /**
     * Set Chromosome
     * @param kro
     */
    void setChromosome(Chromosome kro);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream &out) const;


    /**
     * Override operator ==
     * @param pro
     * @return
     */
    bool operator==(Prokaryote pro) const;

};


/**
 * Override operator <<
 * @param out
 * @param p
 * @return
 */
ostream & operator<<(ostream &out, const Prokaryote &p);


/**
 * From Prokaryote To json
 * @param j
 * @param p
 */
void to_json(json& j, const Prokaryote& p);


/**
 * From json to Prokaryote
 * @param j
 * @param p
 */
void from_json(const json& j, Prokaryote& p);

#endif //BDDCELL_PROKARYOTE_H
