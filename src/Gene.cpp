//
// Created by visenya on 20/11/17.
//

#include "Gene.h"

//Constructor

Gene::Gene(string nameGene, string locus) {
    this->nameGene = nameGene;
    this->locus = locus;
}


// Getter and Setter

string Gene::getNameGene() const {
    return this->nameGene;
}

string Gene::getLocus() const {
    return this->locus;
}

void Gene::setNameGene(string name) {
    this->nameGene = name;
}

void Gene::setLocus(string locus) {
    this->locus = locus;
}


// Display

void Gene::affiche(ostream &out) const {
    out << "Gene : NameGene = " << this->nameGene << ", Locus = " << this->locus << "." << endl;
}


// Override operator

bool Gene::operator==(Gene gene) const {
    return (this->nameGene == gene.nameGene) && (this->locus == gene.nameGene);
}

ostream & operator<<(ostream &out, const Gene &g){
    g.affiche(out);
    return out;
}


// Export and Import

void to_json(json& j, const Gene& g) {
    j = json{{"nameGene", g.getNameGene()}, {"locus", g.getLocus()}};
}

void from_json(const json& j, Gene& g) {
    g.setNameGene(j.at("nameGene").get<std::string>());
    g.setLocus(j.at("locus").get<std::string>());
}