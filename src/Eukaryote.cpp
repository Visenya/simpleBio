//
// Created by visenya on 16/11/17.
//

#include "Eukaryote.h"

//Constructor

Eukaryote::Eukaryote(string id, double size, string nameOrgane): Cell(id, size), nameOrgane(nameOrgane){
    this->type = "Eukaryote";
}


// Getter and Setter

string Eukaryote::getNameOrgane() const {
    return this->nameOrgane;
}

list<Chromosome> Eukaryote::getArrayChromosome() const {
    return this->arrayKro;
}

void Eukaryote::setNameOrgane(string nameOrgane) {
    this->nameOrgane = nameOrgane;
}

void Eukaryote::setArrayChromosome(list<Chromosome> arrayKro) {
    this->arrayKro = arrayKro;
}


// Display

void Eukaryote::affiche(ostream &out) const {
    out << "**********************************************************************\n" << endl;
    out << "Eukaryotic Cell" << endl;
    Cell::affiche(out);
    out << " - Organe's Name = " << this->nameOrgane << "." << endl;
    if (!this->arrayKro.empty()) {
        out << " - Array of Chromosome : " << endl;

        for (auto const &a : arrayKro)
            out << "\t" << a;
    }
    out << "\n**********************************************************************" << endl;
}


// Function

void Eukaryote::addChromosome(Chromosome kro) {
    this->arrayKro.push_back(kro);
}


// Copy Constructor

Eukaryote::Eukaryote(const Eukaryote &euk) {
    this->id = euk.id;
    this->size = euk.size;
    this->nameOrgane = euk.nameOrgane;
    this->arrayKro = euk.arrayKro;
    this->arrayProtein = euk.arrayProtein;
}


// Override operator

ostream & operator<<(ostream &out, const Eukaryote &p){
    p.affiche(out);
    return out;
}

bool Eukaryote::operator==(Eukaryote euk) const {
    return (this->id == euk.id) && (this->size == euk.size) && (this->arrayProtein == euk.arrayProtein) && (this->arrayKro == euk.arrayKro);

}


// Export and Import

void to_json(json& j, const Eukaryote& e) {
    j = json{{"idCell", e.getIdName()}, {"sizeCell", e.getSize()}, {"nameOrgane", e.getNameOrgane()}, {"arrayProtein", e.getArrayProtein()}, {"arrayChromosome", e.getArrayChromosome()}};
}

void from_json(const json& j, Eukaryote& e) {
    e.setIdName(j.at("idCell").get<std::string>());
    e.setSize(j.at("sizeCell").get<double>());
    e.setNameOrgane(j.at("nameOrgane").get<std::string>());
    e.setArrayProtein(j.at("arrayProtein").get<std::list<Protein>>());
    e.setArrayChromosome(j.at("arrayChromosome").get<std::list<Chromosome>>());
}