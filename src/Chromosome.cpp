//
// Created by visenya on 16/11/17.
//

#include "Chromosome.h"

//Constructor

Chromosome::Chromosome(string nameChromosome, string type) {
    this->nameChromosome = nameChromosome;
    this->type = type;
}


// Getter and Setter

string Chromosome::getNameChromosome() const {
    return this->nameChromosome;
}

string Chromosome::getType() const {
    return  this->type;
}

list<Gene> Chromosome::getArrayGene() const {
    return this->arrayGene;
}

void Chromosome::setType(string type) {
    this->type = type;
}

void Chromosome::setNameChromosome(string nameChromosome) {
    this->nameChromosome = nameChromosome;
}

void Chromosome::setArrayGene(list<Gene> arrayGene) {
    this->arrayGene = arrayGene;
}


// Function

void Chromosome::addGene(Gene g) {
    this->arrayGene.push_back(g);
}


// Display

void Chromosome::affiche(ostream & out) const {
    out << " - Chromosome : Name = " << this->nameChromosome << " , Type = " << this->type << "." << endl;
    if (!this->arrayGene.empty()){
        out << "\t\t - Array of Gene : " << endl;
        for ( auto const& a : arrayGene)
            out << "\t\t\t - " << a;
    }
}


// Copy Constructor

Chromosome::Chromosome(const Chromosome& kro){
    this->nameChromosome = kro.nameChromosome;
    this->type = kro.type;
    this->arrayGene = kro.arrayGene;
};


// Override operator

bool Chromosome::operator==(Chromosome kro) const {
    return (this->nameChromosome == kro.nameChromosome) && (this->type == kro.type) && (this->arrayGene == kro.arrayGene);
}

ostream & operator<<(ostream &out, const Chromosome &c){
    c.affiche(out);
    return out;
}


// Export and Import

void to_json(json& j, const Chromosome& k) {
    j = json{{"nameChromosome", k.getNameChromosome()}, {"typeChromosome", k.getType()}, {"arrayGene", k.getArrayGene()}};
}

void from_json(const json& j, Chromosome& k) {
    k.setNameChromosome(j.at("nameChromosome").get<std::string>());
    k.setType(j.at("typeChromosome").get<std::string>());
    k.setArrayGene(j.at("arrayGene").get<std::list<Gene>>());
}