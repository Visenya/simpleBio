//
// Created by visenya on 16/11/17.
//

#include "Prokaryote.h"

//Constructor

Prokaryote::Prokaryote(string id, double size, Chromosome chromosome): Cell(id, size), chro(chromosome){
    this->type = "Prokaryote";
};


// Getter and Setter

Chromosome Prokaryote::getChromosome() const {
    return this->chro;
}

void Prokaryote::setChromosome(Chromosome kro) {
    this->chro = kro;
}


// Display

void Prokaryote::affiche(ostream & out) const {
    out << "**********************************************************************\n" << endl;
    out << "Prokaryote Cell" << endl;
    Cell::affiche(out);
    if (this->chro.getNameChromosome() != "") {
        out << this->chro << endl;
    }
    out << "\n**********************************************************************" << endl;

}


// Override operator

bool Prokaryote::operator==(Prokaryote pro) const {
    return (this->id == pro.id) && (this->size == pro.size) && (this->chro == pro.chro);
}

ostream & operator<<(ostream &out, const Prokaryote &p){
    p.affiche(out);
    return out;
}


// Export and Import

void to_json(json& j, const Prokaryote& e) {
    j = json{{"idCell", e.getIdName()}, {"sizeCell", e.getSize()}, {"arrayProtein", e.getArrayProtein()}, {"chromosome", e.getChromosome()}};
}

void from_json(const json& j, Prokaryote& e) {
    e.setIdName(j.at("idCell").get<std::string>());
    e.setSize(j.at("sizeCell").get<double>());
    e.setChromosome(j.at("chromosome").get<Chromosome>());
    e.setArrayProtein(j.at("arrayProtein").get<std::list<Protein>>());
}