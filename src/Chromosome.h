//
// Created by Aline et Guillaume on 16/11/17.
//

#ifndef BDDCELL_CHROMOSOME_H
#define BDDCELL_CHROMOSOME_H

#include <iostream>
#include <list>
#include "../lib/json.hpp"

using namespace nlohmann;
using namespace std;

#include "Cell.h"
#include "Gene.h"

class Chromosome {

private:
    string nameChromosome;
    string type;
    list<Gene> arrayGene;

public:

    /**
     * Constructor
     * @param nameChromosome
     * @param type
     */
    Chromosome(string nameChromosome = "", string type = "");


    /**
     * Get Name
     * @return name
     */
    string getNameChromosome() const;


    /**
     * Get Type
     * @return type
     */
    string getType() const;


    /**
     * Get array Gene
     * @return array of gene
     */
    list<Gene> getArrayGene() const ;

    /**
     * Set Type
     * @param type
     */
    void setType(string type);


    /**
     * Set Name
     * @param nameChromosome
     */
    void setNameChromosome(string nameChromosome);


    /**
     * Set array Gene
     * @param arrayGene
     */
    void setArrayGene(list<Gene> arrayGene);


    /**
     * Add Gene
     * @param gene
     */
    void addGene(Gene gene);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream & out) const;


    /**
     * Override operator ==
     * @param kro
     * @return boolean
     */
    bool operator==(Chromosome kro) const;


    /**
     * Copy Constructor
     * @param kro
     */
    Chromosome(const Chromosome& kro);

};


/**
 * Override operator <<
 * @param out
 * @param c
 * @return
 */
ostream & operator<<(ostream &out, const Chromosome &c);


/**
 * From Chromosome To json
 * @param j
 * @param k
 */
void to_json(json& j, const Chromosome& k);


/**
 * From json to Chromosome
 * @param j
 * @param k
 */
void from_json(const json& j, Chromosome& k);

#endif //BDDCELL_CHROMOSOME_H
