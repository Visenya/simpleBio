//
// Created by Aline et Guillaume on 02/12/17.
//

#ifndef BDDCELL_MENU_H
#define BDDCELL_MENU_H

#include <string>
#include <iostream>
#include "../lib/json.hpp"
#include "addCell.h"

using namespace std;
using namespace nlohmann;

/**
 * Display the menu
 */
void displayMenu();

/**
 * Creation of the menu
 * @param bdd
 */
void selectMenu(Database bdd);

/**
 * Get path app's execution
 * @return
 */
string getPath();


#endif //BDDCELL_MENU_H
