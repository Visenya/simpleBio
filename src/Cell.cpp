//
// Created by visenya on 09/11/17.
//

#include "Cell.h"

//Constructor

Cell::Cell(string id, double size) {
    this->id = id;
    this->size = size;
}


// Getter and Setter

string Cell::getIdName() const {
    return this->id;
}

double Cell::getSize() const {
    return this->size;
}

string Cell::getType() const {
    return this->type;
}

list<Protein> Cell::getArrayProtein() const {
    return this->arrayProtein;
}

void Cell::setIdName(string idName) {
    this->id = idName;
}

void Cell::setSize(double size) {
    this->size = size;
}

void Cell::setArrayProtein(list<Protein> arrayProtein) {
    this->arrayProtein = arrayProtein;
}


// Display

void Cell::affiche(ostream & out) const {
    out << " - " << "Id = " << this->id << "." << endl;
    out << " - " << "Size = " << this->size << "." << endl;
    if (!this->arrayProtein.empty()) {
        out << " - " << "Array of Protein : " << endl;
        for (auto const &a : arrayProtein)
            out << "\t - " << a;
    }
}


// Copy Constructor

Cell::Cell(const Cell &cell) {
    this->id = cell.id;
    this->size = cell.size;
    this->arrayProtein = cell.arrayProtein;
}


// Function

void Cell::addProtein(Protein protein) {
    this->arrayProtein.push_back(protein);
}


// Override operator

ostream & operator<<(ostream &out, const Cell &c){
    c.affiche(out);
    return out;
}
