//
// Created by Aline et Guillaume on 09/11/17.
//

#ifndef BDDCELL_CELL_H
#define BDDCELL_CELL_H

#include <string>
#include <iostream>
#include <list>
#include "../lib/json.hpp"
#include "Protein.h"

using namespace nlohmann;
using namespace std;

class Cell {

protected:
    string id;
    double size;
    list<Protein> arrayProtein;
    string type;

public:

    /**
     * Constructor
     * @param id
     * @param size
     */
    Cell(string id = "", double size = 0);


    /**
     * Get Id Cell
     * @return idName
     */
    string getIdName() const;


    /**
     * Get size Cell
     * @return size
     */
    double getSize() const;


    /**
     * Set IdName
     * @param IdName
     */
    void setIdName(string idName);


    /**
     * Set size
     * @param size
     */
    void setSize(double size);


    /**
     * Set Array Protein
     * @param arrayProtein
     */
    void setArrayProtein(list<Protein> arrayProtein);


    /**
     * Get size Cell
     * @return arrayProtein
     */
    list<Protein> getArrayProtein() const;


    /**
     * Display Cell
     */
    virtual void affiche(ostream &) const;


    /**
     * Add protein
     * @param p
     */
    void addProtein(Protein p);


    /**
     * Get Type
     * @return type
     */
    string getType() const;


    /**
     * Copy Constructor
     * @param cell
     */
    Cell(const Cell& cell);

};


/**
 * Redefinition of << (display)
 * @param out
 * @param c
 * @return
 */
ostream & operator<<(ostream &out, const Cell &c);



/*void to_json(json& j, const Cell& e);

void from_json(const json& j, Cell& e);*/

#endif //BDDCELL_CELL_H
