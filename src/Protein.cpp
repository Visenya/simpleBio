//
// Created by visenya on 09/11/17.
//

#include "Protein.h"

//Constructor

Protein::Protein(string id, string name) {
    this->id = id;
    this->name = name;
}


// Getter and Setter

string Protein::getIdProtein() const {
    return this->id;
}

void Protein::setIdProtein(string id) {
    this->id = id;
}

void Protein::setNameProtein(string name) {
    this->name = name;
}

string Protein::getNameProtein() const {
    return this->name;
}


// Display

void Protein::affiche(ostream & out) const {
    out << "Protein : Id = " << this->id << ", Name = " << this->name << "." << endl;
}


// Override operator

bool Protein::operator==(Protein prot) const {
    return (this->id == prot.id) && (this->name == prot.name);
}

ostream & operator<<(ostream &out, const Protein &p){
    p.affiche(out);
    return out;
}


// Export and Import

void to_json(json& j, const Protein& p) {
    j = json{{"idProtein", p.getIdProtein()}, {"nameProtein", p.getNameProtein()}};
}

void from_json(const json& j, Protein& e) {
    e.setIdProtein(j.at("idProtein").get<std::string>());
    e.setNameProtein(j.at("nameProtein").get<std::string>());
}